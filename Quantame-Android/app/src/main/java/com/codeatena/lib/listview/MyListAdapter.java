package com.codeatena.lib.listview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import llvc.app.quantame.R;
import llvc.app.quantame.model.Cell;
import llvc.app.quantame.model.Output;
import llvc.app.quantame.model.Section;

public class MyListAdapter extends ArrayAdapter<MyListItem> {

    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MyListItem item = getItem(position);

        if (item instanceof MyListItem) {
            view = inflater.inflate(item.layoutID, parent, false);
            Object data = item.data;

            if (data instanceof Output) {
                TextView tvDate = (TextView) view.findViewById(R.id.date_textView);
                LinearLayout llSections = (LinearLayout) view.findViewById(R.id.sections_linearLayout);

                Output output = (Output) data;
                for (Section section: output.arrSections) {
                    View viewSection = inflater.inflate(R.layout.row_section, parent, false);
                    TextView tvSection = (TextView) viewSection.findViewById(R.id.section_textView);
                    LinearLayout llCells = (LinearLayout) viewSection.findViewById(R.id.cells_linearLayout);

                    tvSection.setText(section.title);
                    for (Cell cell: section.arrCells) {
                        View viewCell = inflater.inflate(R.layout.row_cell, parent, false);
                        TextView tvKey = (TextView) viewCell.findViewById(R.id.key_textView);
                        TextView tvValue = (TextView) viewCell.findViewById(R.id.value_textView);
                        tvKey.setText(cell.key + ":");
                        tvValue.setText(cell.value);
                        llCells.addView(viewCell);
                    }
                    llSections.addView(viewSection);
                }
                tvDate.setText(output.date);
            }
        }

        return view;
    }
}