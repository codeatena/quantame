package llvc.app.quantame.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import llvc.app.quantame.R;

public class LoginActivity extends Activity {

    EditText etUsername;
    EditText etPassword;
    Button btnSignin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        etUsername = (EditText) findViewById(R.id.username_editText);
        etPassword = (EditText) findViewById(R.id.password_editText);
        btnSignin = (Button) findViewById(R.id.signin_button);
    }

    private void initValue() {

    }

    private void initEvent() {
        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goMain();
            }
        });
    }

    private void goMain() {
        startActivity(new Intent(this, MainActivity.class));
    }
}
