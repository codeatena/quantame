package llvc.app.quantame;

import java.util.ArrayList;

import llvc.app.quantame.model.Output;

/**
 * Created by User on 1/25/2016.
 */
public class GlobalData {

    public static GlobalData instance;

    public String strSampleData1 = "{\n" +
            "'input': '7 minute workout',\n" +
            "'output': {\n" +
            "'exercise': {\n" +
            "'duration': '420'\n" +
            "},\n" +
            "'alcohol': {\n" +
            "'description': 'wine',\n" +
            "'grams': '28'\n" +
            "}\n" +
            "},\n" +
            "'isCurrentTime':true,\n" +
            "'statuscode':201\n" +
            "}";

    public String strSampleData2 = "{\n" +
            "'input': 'Medication adderall 10 milligrams',\n" +
            "'output': {\n" +
            "'medication': {\n" +
            "'description':'adderall',\n" +
            "'dosage':'10',\n" +
            "'dosage_units':'milligrams'\n" +
            "}\n" +
            "},\n" +
            "'isCurrentTime':true,\n" +
            "'statuscode':201\n" +
            "}";

    public String strSampleData3 = "{\n" +
            "'input': 'Medication tylenol at 10 this morning',\n" +
            "'output': {\n" +
            "'medication': {\n" +
            "'description':'tylenol'\n" +
            "}\n" +
            "},\n" +
            "'isCurrentTime':false,\n" +
            "'statuscode':201\n" +
            "}";

    public static GlobalData getInstance() {
        if (instance == null) {
            instance = new GlobalData();
        }
        return instance;
    }

    public GlobalData() {
        arrOutputs = new ArrayList<Output>();
        makeSampleData();
    }

    public ArrayList<Output> arrOutputs;

    public void makeSampleData() {

    }
}