package llvc.app.quantame.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;

import com.codeatena.lib.listview.MyListAdapter;
import com.codeatena.lib.listview.MyListItem;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import junit.framework.Assert;

import java.util.ArrayList;

import llvc.app.quantame.GlobalData;
import llvc.app.quantame.R;
import llvc.app.quantame.model.Output;

public class HistoryActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<MyListItem> arrItems;
    MyListAdapter myListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        listView = (ListView) findViewById(R.id.listView);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initValue() {
        arrItems = new ArrayList<MyListItem>();

        JsonObject jsonObject1 = (new JsonParser()).parse(GlobalData.getInstance().strSampleData1).getAsJsonObject();
        Output output1 = new Output(jsonObject1);
        arrItems.add(new MyListItem(R.layout.row_output, output1));

        JsonObject jsonObject2 = (new JsonParser()).parse(GlobalData.getInstance().strSampleData2).getAsJsonObject();
        Output output2 = new Output(jsonObject2);
        arrItems.add(new MyListItem(R.layout.row_output, output2));

        JsonObject jsonObject3 = (new JsonParser()).parse(GlobalData.getInstance().strSampleData3).getAsJsonObject();
        Output output3 = new Output(jsonObject3);
        arrItems.add(new MyListItem(R.layout.row_output, output3));

        myListAdapter = new MyListAdapter(this, arrItems);
        listView.setAdapter(myListAdapter);
    }

    private void initEvent() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}