package llvc.app.quantame.model;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by User on 1/25/2016.
 */
public class Section {

    public String title;
    public ArrayList<Cell> arrCells;

    public Section(String _title, JsonObject jsonValue) {
        title = _title;
        arrCells = new ArrayList<Cell>();

        Set<Map.Entry<String, JsonElement>> esValue = jsonValue.entrySet();
        Iterator<Map.Entry<String, JsonElement>> itValue = esValue.iterator();

        while (itValue.hasNext()) {
            Map.Entry<String, JsonElement> etValue = itValue.next();
            Cell cell = new Cell(etValue.getKey(), etValue.getValue().getAsString());
            arrCells.add(cell);
        }
    }
}