package llvc.app.quantame.model;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Created by User on 1/25/2016.
 */
public class Output {

    public String date;
    public ArrayList<Section> arrSections;

    public Output(JsonObject jsonObject) {
        date = "Jan 21st 10:18 am";
        arrSections = new ArrayList<Section>();

        JsonObject jsonOutput = jsonObject.getAsJsonObject("output");
        Set<Entry<String, JsonElement>> esOutput = jsonOutput.entrySet();
        Iterator<Entry<String, JsonElement>> itOutout = esOutput.iterator();

        while (itOutout.hasNext()) {
            Entry<String, JsonElement> etOutput = itOutout.next();
            JsonObject jsonValue = etOutput.getValue().getAsJsonObject();
            Section section = new Section(etOutput.getKey(), jsonValue);
            arrSections.add(section);
        }
    }
}