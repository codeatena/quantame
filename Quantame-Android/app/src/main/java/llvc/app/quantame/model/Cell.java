package llvc.app.quantame.model;

/**
 * Created by User on 1/25/2016.
 */
public class Cell {
    public String key;
    public String value;

    public Cell(String _key, String _value) {
        key = _key;
        value = _value;
    }
}
